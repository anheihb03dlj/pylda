# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Xls.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_XlsDialog(object):
    def setupUi(self, XlsDialog):
        XlsDialog.setObjectName(_fromUtf8("XlsDialog"))
        XlsDialog.resize(354, 173)
        XlsDialog.setModal(False)
        self.buttonBox = QtGui.QDialogButtonBox(XlsDialog)
        self.buttonBox.setGeometry(QtCore.QRect(110, 120, 141, 71))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.xlsFileEdit = QtGui.QLineEdit(XlsDialog)
        self.xlsFileEdit.setGeometry(QtCore.QRect(10, 30, 271, 21))
        self.xlsFileEdit.setReadOnly(True)
        self.xlsFileEdit.setObjectName(_fromUtf8("xlsFileEdit"))
        self.headerSpinxBox = QtGui.QSpinBox(XlsDialog)
        self.headerSpinxBox.setGeometry(QtCore.QRect(280, 120, 71, 21))
        self.headerSpinxBox.setProperty("value", 7)
        self.headerSpinxBox.setObjectName(_fromUtf8("headerSpinxBox"))
        self.sheetNameEdit = QtGui.QLineEdit(XlsDialog)
        self.sheetNameEdit.setGeometry(QtCore.QRect(280, 70, 71, 21))
        self.sheetNameEdit.setReadOnly(False)
        self.sheetNameEdit.setObjectName(_fromUtf8("sheetNameEdit"))
        self.label_2 = QtGui.QLabel(XlsDialog)
        self.label_2.setGeometry(QtCore.QRect(240, 120, 51, 21))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.groupBox = QtGui.QGroupBox(XlsDialog)
        self.groupBox.setGeometry(QtCore.QRect(10, 60, 221, 81))
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.nonSynRadio = QtGui.QRadioButton(self.groupBox)
        self.nonSynRadio.setGeometry(QtCore.QRect(10, 50, 201, 21))
        self.nonSynRadio.setObjectName(_fromUtf8("nonSynRadio"))
        self.synRadio = QtGui.QRadioButton(self.groupBox)
        self.synRadio.setGeometry(QtCore.QRect(10, 20, 181, 21))
        self.synRadio.setChecked(True)
        self.synRadio.setObjectName(_fromUtf8("synRadio"))
        self.xlsFileSelBtn = QtGui.QPushButton(XlsDialog)
        self.xlsFileSelBtn.setGeometry(QtCore.QRect(290, 30, 61, 23))
        self.xlsFileSelBtn.setObjectName(_fromUtf8("xlsFileSelBtn"))
        self.label = QtGui.QLabel(XlsDialog)
        self.label.setGeometry(QtCore.QRect(10, 10, 341, 16))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_3 = QtGui.QLabel(XlsDialog)
        self.label_3.setGeometry(QtCore.QRect(240, 70, 41, 21))
        self.label_3.setObjectName(_fromUtf8("label_3"))

        self.retranslateUi(XlsDialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), XlsDialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), XlsDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(XlsDialog)

    def retranslateUi(self, XlsDialog):
        XlsDialog.setWindowTitle(_translate("XlsDialog", "选择xls文件", None))
        self.sheetNameEdit.setText(_translate("XlsDialog", "BSA Export", None))
        self.label_2.setText(_translate("XlsDialog", "标题行", None))
        self.groupBox.setTitle(_translate("XlsDialog", "数据类型", None))
        self.nonSynRadio.setText(_translate("XlsDialog", "非协同测试数据(LDA1,LDA2,LDA3)", None))
        self.synRadio.setText(_translate("XlsDialog", "协同测试数据(U,V,W)", None))
        self.xlsFileSelBtn.setText(_translate("XlsDialog", "选择", None))
        self.label.setText(_translate("XlsDialog", "XLS文件(请确保xls文件数据类型与所选择的\"数据类型\"一致!", None))
        self.label_3.setText(_translate("XlsDialog", "表  名", None))

