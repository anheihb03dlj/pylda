#-*- coding:utf-8 -*-

import sys

from PyQt4 import QtGui, QtCore

from ui_lda import Ui_LDAForm
from ui_xls import Ui_XlsDialog
from ui_data_range import Ui_DataRangeDialog
from ui_random import Ui_RandomDialog

from lda import *

import numpy as np
import pandas as pd
from scipy import stats
import statsmodels.api as sm

"""
http://blog.sina.com.cn/s/blog_4b5039210100gxy0.html
http://www.cnblogs.com/rollenholt/archive/2011/11/16/2251904.html
http://zetcode.com/gui/pyqt4/layoutmanagement/
http://jimmykuu.sinaapp.com/blog/12

pyuic4 -o ui_widget.py widget.class

from PyQt4 import QtGui, QtCore
from ui_widget import Ui_Form
 
class Widget(QtGui.QWidget, Ui_Form):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.setupUi(self)
 
    @QtCore.pyqtSignature("")
    def on_pbHello_clicked(self):
        self.lHello.setText('Hello PyQt4')
 
if __name__ == '__main__':
    import sys
    app = QtGui.QApplication(sys.argv)
    widget = Widget()
    widget.show()
    sys.exit(app.exec_())
"""

class RandomDialog(QtGui.QDialog, Ui_RandomDialog):
    def __init__( self ):
        super( RandomDialog, self ).__init__()
        #self.resize( 500, 500 )
        self.setupUi(self)

    def getDatas(self):
        N = self.sampleSpinxBox.value()
        syn = self.synRadio.isChecked()
        if syn:
            #记录速度所在的列
            velocity_colums = [u'U', u'V', u'W']
            #记录时间所在的列
            #time_colums = [u'AT', u'TT']
            time_colums = [u'AT']
        else:
            #记录速度所在的列
            velocity_colums = [u'LDA1', u'LDA2', u'LDA3']
            #记录时间所在的列
            #time_colums = [u'AT', u'TT', u'AT2', u'TT2', u'AT3', u'TT3']
            time_colums = [u'AT', u'AT2', u'AT3']       
        mu = self.meanSpinBox.value()
        sigma = self.stdSpinBox.value()
        freedom = self.freedomSpinBox.value()
        dist_type = u'random'
        if self.noramRadio.isChecked():
            dist_type = u'norm'
        if self.tRadio.isChecked():
            dist_type = u't'
        return N, dist_type, mu, sigma, freedom, velocity_colums, time_colums


class DataRangeDialog( QtGui.QDialog, Ui_DataRangeDialog ):
    def __init__( self ):
        super( DataRangeDialog, self ).__init__()
        #self.resize( 500, 500 )
        self.setupUi(self)
        self.colorComboBox.addItems(['red', 'blue', 'green', 'cyan', 'magenta', 'yellow', 'black', 'white'])
        self.linestyleComboBox.addItems(['-', '--', ':'])

    def getDatas(self):
        first = self.startSpinxBox.value()
        last = self.endSpinxBox.value()
        step = self.StepSpinxBox.value()
        color = str(self.colorComboBox.currentText())
        linestyle = str(self.linestyleComboBox.currentText())
        linewidth = self.lineWidthSpinBox.value()
        return first, last, step, color, linestyle, linewidth

class XlsDialog( QtGui.QDialog, Ui_XlsDialog ):
    def __init__( self ):
        super( XlsDialog, self ).__init__()
        #self.resize( 500, 500 )
        self.setupUi(self)
        self.xlsFileSelBtn.clicked.connect(self.selectXlsFile)

    def selectXlsFile(self):
        fileName = QtGui.QFileDialog.getOpenFileName(self, u"打开xls文件", "", u"Xls文件 (*.xls)")
        if not fileName.isEmpty():
            self.xlsFileEdit.setText( fileName )

    def getDatas(self):
        #注意QString要转换成python string
        xls_file = str(self.xlsFileEdit.text())
        colnames = None
        syn = self.synRadio.isChecked()
        sheet_name = str(self.sheetNameEdit.text())
        header = self.headerSpinxBox.value()
        if syn:
            #记录速度所在的列
            velocity_colums = [u'U', u'V', u'W']
            #记录时间所在的列
            #time_colums = [u'AT', u'TT']
            time_colums = [u'AT']
        else:
            #记录速度所在的列
            velocity_colums = [u'LDA1', u'LDA2', u'LDA3']
            #记录时间所在的列
            #time_colums = [u'AT', u'TT', u'AT2', u'TT2', u'AT3', u'TT3']
            time_colums = [u'AT', u'AT2', u'AT3']
        return xls_file, sheet_name, header, velocity_colums, time_colums

"""
实现python的控制台输出(stdout, print)重定向功能
http://stackoverflow.com/questions/19838680/pyqt-displaying-python-interpreter-output?lq=1
http://stackoverflow.com/questions/8356336/how-to-capture-output-of-pythons-interpreter-and-show-in-a-text-widget/8356465#8356465


中文编码问题
http://www.cnblogs.com/babykick/archive/2011/05/16/2048155.html
http://www.educity.cn/develop/667502.html
http://blog.csdn.net/nwpulei/article/details/8581678
http://www.cnblogs.com/dkblog/archive/2011/03/02/1980644.html
http://www.tuicool.com/articles/uQ3qm2Y

qt和Python的signal/slot关系
http://stackoverflow.com/questions/2048098/pyqt4-signals-and-slots

"""
class EmittingStream(QtCore.QObject):
    textWritten = QtCore.pyqtSignal(str)
    def write(self, text):
        # self.textWritten.emit(text)
        if isinstance(text, unicode):
            self.textWritten.emit(text)
        else:
            self.textWritten.emit(text.decode('utf-8'))

class Window( QtGui.QWidget, Ui_LDAForm ):
    def __init__( self ):
        super( Window, self ).__init__()
        self.setupUi(self)
        #self.setWindowTitle( "LDA Data Analyzer" )
        #self.resize( 500, 500 )

        self.clearLogBtn.clicked.connect(self.clearLog)
        self.xlsBtn.clicked.connect(self.genDataFromXlsFile)
        self.statsticBtn.clicked.connect(self.printStatsInfo)
        self.origPlotBtn.clicked.connect(self.origPlot)
        self.kdePlotBtn.clicked.connect(self.kdePlot)
        self.qqPlotBtn.clicked.connect(self.qqPlot)
        self.ppPlotBtn.clicked.connect(self.ppPlot)
        self.histPlotBtn.clicked.connect(self.histPlot)
        self.ksTestBtn.clicked.connect(self.ksTest)
        self.fftPlotBtn.clicked.connect(self.fftTest)

        self.timePlotBtn.clicked.connect(self.timePlot)
        self.statsPlotBtn.clicked.connect(self.statsPlot)

        self.addRangesBtn.clicked.connect(self.addDataRange)
        self.clearRangesBtn.clicked.connect(self.clearRanges)
        self.viewRangesBtn.clicked.connect(self.viewRanges)

        self.enableDiscretRangeButtons(False)
        self.connect(self.continueRadio, QtCore.SIGNAL("toggled(bool)"), self.continueRadioChanged)
        self.connect(self.descretRadio, QtCore.SIGNAL("toggled(bool)"), self.descretRadioChanged)
        self.connect(self.columnCombo, QtCore.SIGNAL("currentIndexChanged(int)"), self.columnCombo_2, QtCore.SLOT("setCurrentIndex(int)")) 
        
        self.randBtn.clicked.connect(self.randomData)
        #所有数据
        self.df = None
        self.velocity_colums = None
        self.time_colums = None
        self.data_ranges = None

        #stdout重定向
        self.stream = EmittingStream(textWritten=self.logMessage)
        sys.stdout = self.stream
        sys.stderr = self.stream

    def __del__(self):
        #恢复原来的系统stdout
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__

    def enableDiscretRangeButtons(self, checked):
        self.addRangesBtn.setEnabled(checked)
        self.clearRangesBtn.setEnabled(checked)
        self.viewRangesBtn.setEnabled(checked)

    def continueRadioChanged(self, checked):
        self.enableDiscretRangeButtons(not checked)

    def descretRadioChanged(self, checked):
        self.enableDiscretRangeButtons(checked)

    def updateUI(self):
        print u'用户选择的速度列:',self.velocity_colums
        print u'用户选择的时间列:',self.time_colums
        #添加所有的column到combox中,由用户选择分析哪一类数据
        self.columnCombo.clear()
        self.columnCombo_2.clear()
        self.columnCombo.addItems(self.velocity_colums)
        self.columnCombo_2.addItems(self.time_colums)
        #更新数据的最大个数
        n = len(self.df.index)
        self.startSpinxBox.setValue(0)
        self.endSpinxBox.setValue(n)
        self.startSpinxBox_2.setValue(0)
        self.endSpinxBox_2.setValue(n)

    def resetData(self):
        del self.df
        del self.velocity_colums
        del self.time_colums
        del self.data_ranges
        self.df = None
        self.velocity_colums = None
        self.time_colums = None
        self.data_ranges = None

    def randomData(self):
        dlg = RandomDialog()
        if dlg.exec_() == QtGui.QDialog.Accepted:
            #重置所有数据
            self.resetData()
            #从对话框中获取数据
            N, dist_type, mu, sigma, freedom, velocity_colums, time_colums = dlg.getDatas()
            #生成速度数据
            datas = []
            colnames = []
            for vcol in velocity_colums:
                colnames.append(vcol)
                d = generate_random_data(N, dist_type=dist_type, mu=mu, sigma=sigma, freedom=freedom)
                datas.append(d)
            for tcol in time_colums:
                colnames.append(tcol)
                d = np.linspace(0, 500000, N)
                datas.append(d)                
            #生成DataFrame数据结构(通过词典)
            self.df = pd.DataFrame(dict(zip(colnames, datas)))
            print velocity_colums
            print time_colums
            #记录速度所在的列
            self.velocity_colums = velocity_colums
            #记录时间所在的列
            self.time_colums = time_colums            
            #更新界面
            self.updateUI()

    def genDataFromXlsFile(self):
        dlg = XlsDialog()
        if dlg.exec_() == QtGui.QDialog.Accepted:
            #重置所有数据
            self.resetData()            
            #从对话框中读取数据
            xls_file, sheet_name, header, velocity_colums, time_colums = dlg.getDatas()
            try:
                #从xls中读取数据,返回一个DataFrame对象
                self.df = read_excel_data(xls_file, sheet_name, header)
                #记录速度所在的列
                self.velocity_colums = velocity_colums
                #记录时间所在的列
                self.time_colums = time_colums
                #更新界面
                self.updateUI()
            except Exception, e:
                self.df = None
                self.velocity_colums = None
                self.time_colums = None
                self.logMessage(str(e))
                #self.logMessage('Please make sure params of xls file are corrrect!')
    
    def getVelocityDatas(self):
        if self.df is None or len(self.df)==0:
            QtGui.QMessageBox.information( self, u"提示", u"请选择xls文件读取数据或生成一组随机数据!")
        vcol = str(self.columnCombo.currentText())        
        first = self.startSpinxBox.value()
        last = self.endSpinxBox.value()
        return self.df[vcol][first:last]

    def getTimeDatas(self):
        if self.df is None or len(self.df)==0:
            QtGui.QMessageBox.information( self, u"提示", u"请选择xls文件读取数据或生成一组随机数据!")
        tcol = str(self.columnCombo_2.currentText())
        first = self.startSpinxBox_2.value()
        last = self.endSpinxBox_2.value()
        return self.df[tcol][first:last]

    def clearLog(self):
        self.textEdit.clear()

    def logMessage(self, text):
        cursor = self.textEdit.textCursor()
        cursor.movePosition(QtGui.QTextCursor.End)
        cursor.insertText(text)
        self.textEdit.setTextCursor(cursor)
        self.textEdit.ensureCursorVisible()

    def printStatsInfo(self):
        ts = self.getVelocityDatas()
        print_statstic(ts)

    def origPlot(self):
        ts = self.getVelocityDatas()
        simple_plot(ts)

    def kdePlot(self):
        ts = self.getVelocityDatas()    
        kde_plot(ts)

    def qqPlot(self):
        ts = self.getVelocityDatas()
        qq_plot(ts)
    
    def ppPlot(self):
        ts = self.getVelocityDatas()     
        pp_plot(ts)

    def histPlot(self):
        ts = self.getVelocityDatas()
        bins = self.histBinsSpinxBox.value()   
        hist_plot(ts, bins=bins)

    def ksTest(self):
        ts = self.getVelocityDatas()      
        mu = self.meanSpinBox.value()
        sigma = self.stdSpinBox.value()
        ks_test_normal(ts, mu, sigma)

    def fftTest(self):
        ts = self.getVelocityDatas()    
        fft_test(ts)

    def timePlot(self):
        T = self.getTimeDatas()
        step = self.timeStepSpinxBox.value()
        time_statstic_plot(T, step=step)

    def addDataRange(self):
        if self.descretRadio.isChecked():                
            dlg = DataRangeDialog()
            dlg.startSpinxBox.setValue(self.startSpinxBox.value())
            dlg.endSpinxBox.setValue(self.endSpinxBox.value())
            if dlg.exec_() == QtGui.QDialog.Accepted:
                if self.data_ranges is None:
                    self.data_ranges = []
                first, last, step, color, linestyle, linewidth = dlg.getDatas()
                self.data_ranges.append(dict(first=first, last=last, step=step, color=color, linewidth=linewidth, linestyle=linestyle))
                print u'增加离散区间:'
                print u'    开始位置:',first
                print u'    结束位置:',last
                print u'    步长位置:',step
                print u'    颜    色:',color
                print u'    线    型:',linestyle
                print u'    线    宽:',linewidth

    def clearRanges(self):
        del self.data_ranges
        self.data_ranges = None
        print u'清空所有的离散区间数据'

    def viewRanges(self):
        if self.data_ranges is None or len(self.data_ranges)==0:
            print u'尚未定义,请点击<增加>按钮增加离散区间!'
            return

        for i, data_range in enumerate(self.data_ranges):
            print u'第%d个离散区间:' % i+1
            print u'    开始位置:',data_range[u'first']
            print u'    结束位置:',data_range[u'last']
            print u'    步长位置:',data_range[u'step']
            print u'    颜    色:',data_range[u'color']
            print u'    线    型:',data_range[u'linestyle']
            print u'    线    宽:',data_range[u'linewidth']

    def statsPlot(self):
        stats_funcs=[]
        if self.ksCheckBox.isChecked():
            stats_funcs.append(pvalue_f)
        if self.meanCheckBox.isChecked():
            stats_funcs.append(mean_f)
        if self.stdCheckBox.isChecked():
            stats_funcs.append(std_f)
        if len(stats_funcs) == 0:
            print u'请至少选择一个统计量进行分析...'
            return
        ts = self.getVelocityDatas()   
        bPlotInOne = self.onePlotCheckBox.isChecked()
        step = self.statsStepSpinxBox.value()
        stats_var_plot(ts, stats_funcs, step=step, bPlotInOne=bPlotInOne, data_ranges=self.data_ranges)

#主函数
def gui_main():
    app = QtGui.QApplication( sys.argv )
    win = Window()
    win.show()
    sys.exit(app.exec_())


#主函数入口
if __name__=="__main__":
    gui_main()
