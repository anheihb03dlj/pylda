# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'DataRange.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_DataRangeDialog(object):
    def setupUi(self, DataRangeDialog):
        DataRangeDialog.setObjectName(_fromUtf8("DataRangeDialog"))
        DataRangeDialog.resize(309, 114)
        self.buttonBox = QtGui.QDialogButtonBox(DataRangeDialog)
        self.buttonBox.setGeometry(QtCore.QRect(60, 80, 161, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.startSpinxBox = QtGui.QSpinBox(DataRangeDialog)
        self.startSpinxBox.setGeometry(QtCore.QRect(41, 11, 54, 20))
        self.startSpinxBox.setMaximum(50000)
        self.startSpinxBox.setProperty("value", 0)
        self.startSpinxBox.setObjectName(_fromUtf8("startSpinxBox"))
        self.endSpinxBox = QtGui.QSpinBox(DataRangeDialog)
        self.endSpinxBox.setGeometry(QtCore.QRect(140, 10, 54, 20))
        self.endSpinxBox.setMaximum(50000)
        self.endSpinxBox.setProperty("value", 50000)
        self.endSpinxBox.setObjectName(_fromUtf8("endSpinxBox"))
        self.label_11 = QtGui.QLabel(DataRangeDialog)
        self.label_11.setGeometry(QtCore.QRect(210, 10, 24, 16))
        self.label_11.setObjectName(_fromUtf8("label_11"))
        self.StepSpinxBox = QtGui.QSpinBox(DataRangeDialog)
        self.StepSpinxBox.setGeometry(QtCore.QRect(240, 10, 61, 20))
        self.StepSpinxBox.setMaximum(50000)
        self.StepSpinxBox.setProperty("value", 100)
        self.StepSpinxBox.setObjectName(_fromUtf8("StepSpinxBox"))
        self.label_3 = QtGui.QLabel(DataRangeDialog)
        self.label_3.setGeometry(QtCore.QRect(11, 11, 24, 16))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_4 = QtGui.QLabel(DataRangeDialog)
        self.label_4.setGeometry(QtCore.QRect(110, 10, 24, 16))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.label_12 = QtGui.QLabel(DataRangeDialog)
        self.label_12.setGeometry(QtCore.QRect(10, 50, 24, 16))
        self.label_12.setObjectName(_fromUtf8("label_12"))
        self.colorComboBox = QtGui.QComboBox(DataRangeDialog)
        self.colorComboBox.setGeometry(QtCore.QRect(40, 50, 51, 20))
        self.colorComboBox.setObjectName(_fromUtf8("colorComboBox"))
        self.linestyleComboBox = QtGui.QComboBox(DataRangeDialog)
        self.linestyleComboBox.setGeometry(QtCore.QRect(140, 50, 51, 20))
        self.linestyleComboBox.setObjectName(_fromUtf8("linestyleComboBox"))
        self.label_13 = QtGui.QLabel(DataRangeDialog)
        self.label_13.setGeometry(QtCore.QRect(110, 50, 24, 16))
        self.label_13.setObjectName(_fromUtf8("label_13"))
        self.label_14 = QtGui.QLabel(DataRangeDialog)
        self.label_14.setGeometry(QtCore.QRect(210, 50, 24, 16))
        self.label_14.setObjectName(_fromUtf8("label_14"))
        self.lineWidthSpinBox = QtGui.QDoubleSpinBox(DataRangeDialog)
        self.lineWidthSpinBox.setGeometry(QtCore.QRect(240, 50, 60, 20))
        self.lineWidthSpinBox.setMinimum(1.0)
        self.lineWidthSpinBox.setSingleStep(0.1)
        self.lineWidthSpinBox.setProperty("value", 1.0)
        self.lineWidthSpinBox.setObjectName(_fromUtf8("lineWidthSpinBox"))

        self.retranslateUi(DataRangeDialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), DataRangeDialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), DataRangeDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(DataRangeDialog)

    def retranslateUi(self, DataRangeDialog):
        DataRangeDialog.setWindowTitle(_translate("DataRangeDialog", "Dialog", None))
        self.label_11.setText(_translate("DataRangeDialog", "步长", None))
        self.label_3.setText(_translate("DataRangeDialog", "开始", None))
        self.label_4.setText(_translate("DataRangeDialog", "结束", None))
        self.label_12.setText(_translate("DataRangeDialog", "颜色", None))
        self.label_13.setText(_translate("DataRangeDialog", "线型", None))
        self.label_14.setText(_translate("DataRangeDialog", "线宽", None))

