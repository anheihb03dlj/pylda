# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Random.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_RandomDialog(object):
    def setupUi(self, RandomDialog):
        RandomDialog.setObjectName(_fromUtf8("RandomDialog"))
        RandomDialog.resize(407, 290)
        self.groupBox = QtGui.QGroupBox(RandomDialog)
        self.groupBox.setGeometry(QtCore.QRect(10, 130, 381, 131))
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.freedomSpinBox = QtGui.QDoubleSpinBox(self.groupBox)
        self.freedomSpinBox.setGeometry(QtCore.QRect(180, 100, 62, 22))
        self.freedomSpinBox.setMinimum(-99.0)
        self.freedomSpinBox.setProperty("value", 1.0)
        self.freedomSpinBox.setObjectName(_fromUtf8("freedomSpinBox"))
        self.label_11 = QtGui.QLabel(self.groupBox)
        self.label_11.setGeometry(QtCore.QRect(80, 100, 101, 16))
        self.label_11.setObjectName(_fromUtf8("label_11"))
        self.avgRadio = QtGui.QRadioButton(self.groupBox)
        self.avgRadio.setGeometry(QtCore.QRect(10, 30, 89, 16))
        self.avgRadio.setChecked(False)
        self.avgRadio.setObjectName(_fromUtf8("avgRadio"))
        self.meanSpinBox = QtGui.QDoubleSpinBox(self.groupBox)
        self.meanSpinBox.setGeometry(QtCore.QRect(160, 60, 62, 22))
        self.meanSpinBox.setMinimum(-99.0)
        self.meanSpinBox.setProperty("value", 0.0)
        self.meanSpinBox.setObjectName(_fromUtf8("meanSpinBox"))
        self.stdSpinBox = QtGui.QDoubleSpinBox(self.groupBox)
        self.stdSpinBox.setGeometry(QtCore.QRect(310, 60, 62, 22))
        self.stdSpinBox.setMinimum(-99.0)
        self.stdSpinBox.setProperty("value", 1.0)
        self.stdSpinBox.setObjectName(_fromUtf8("stdSpinBox"))
        self.noramRadio = QtGui.QRadioButton(self.groupBox)
        self.noramRadio.setGeometry(QtCore.QRect(10, 60, 89, 16))
        self.noramRadio.setChecked(True)
        self.noramRadio.setObjectName(_fromUtf8("noramRadio"))
        self.label_10 = QtGui.QLabel(self.groupBox)
        self.label_10.setGeometry(QtCore.QRect(230, 60, 101, 16))
        self.label_10.setObjectName(_fromUtf8("label_10"))
        self.tRadio = QtGui.QRadioButton(self.groupBox)
        self.tRadio.setGeometry(QtCore.QRect(10, 100, 89, 16))
        self.tRadio.setObjectName(_fromUtf8("tRadio"))
        self.label_9 = QtGui.QLabel(self.groupBox)
        self.label_9.setGeometry(QtCore.QRect(90, 60, 91, 16))
        self.label_9.setObjectName(_fromUtf8("label_9"))
        self.buttonBox = QtGui.QDialogButtonBox(RandomDialog)
        self.buttonBox.setGeometry(QtCore.QRect(100, 260, 161, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.groupBox_2 = QtGui.QGroupBox(RandomDialog)
        self.groupBox_2.setGeometry(QtCore.QRect(10, 10, 381, 111))
        self.groupBox_2.setObjectName(_fromUtf8("groupBox_2"))
        self.sampleSpinxBox = QtGui.QSpinBox(self.groupBox_2)
        self.sampleSpinxBox.setGeometry(QtCore.QRect(90, 20, 61, 21))
        self.sampleSpinxBox.setMaximum(50000)
        self.sampleSpinxBox.setProperty("value", 50000)
        self.sampleSpinxBox.setObjectName(_fromUtf8("sampleSpinxBox"))
        self.label_8 = QtGui.QLabel(self.groupBox_2)
        self.label_8.setGeometry(QtCore.QRect(10, 20, 91, 16))
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.synRadio = QtGui.QRadioButton(self.groupBox_2)
        self.synRadio.setGeometry(QtCore.QRect(10, 50, 131, 21))
        self.synRadio.setChecked(True)
        self.synRadio.setObjectName(_fromUtf8("synRadio"))
        self.nonSynRadio = QtGui.QRadioButton(self.groupBox_2)
        self.nonSynRadio.setGeometry(QtCore.QRect(10, 80, 201, 21))
        self.nonSynRadio.setObjectName(_fromUtf8("nonSynRadio"))

        self.retranslateUi(RandomDialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), RandomDialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), RandomDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(RandomDialog)

    def retranslateUi(self, RandomDialog):
        RandomDialog.setWindowTitle(_translate("RandomDialog", "随机生成一组速度数据", None))
        self.groupBox.setTitle(_translate("RandomDialog", "分布类型", None))
        self.label_11.setText(_translate("RandomDialog", "自由度(freedom)", None))
        self.avgRadio.setText(_translate("RandomDialog", "均匀分布", None))
        self.noramRadio.setText(_translate("RandomDialog", "正态分布", None))
        self.label_10.setText(_translate("RandomDialog", "均方差(sigma)", None))
        self.tRadio.setText(_translate("RandomDialog", "t分布", None))
        self.label_9.setText(_translate("RandomDialog", "数学期望(mu)", None))
        self.groupBox_2.setTitle(_translate("RandomDialog", "样本参数", None))
        self.label_8.setText(_translate("RandomDialog", "样本数据个数", None))
        self.synRadio.setText(_translate("RandomDialog", "协同测试数据(U,V,W)", None))
        self.nonSynRadio.setText(_translate("RandomDialog", "非协同测试数据(LDA1,LDA2,LDA3)", None))

