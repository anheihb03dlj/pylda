#-*- coding:utf-8 -*-
"""
Python科学计算环境:
(1)安装Anaconda python
Aanaconda集成了众多常用的科学计算包:
    a、scipy, numpy, matplotlib, pandas, statsmodels, patsy, sympy, scikit-learn

另外，我对excel数据进行了修改,由于数据的标题(data2.xls的标题在地6行)采用的符号无法在代码中表示
所以，我把data2.xls中增加了一行，改成普通的英文单词,这样方便程序读取数据

excel中的数据是按照列的方式读取的，比如
df['AT'] = [1, 2, 3, 4]
df['LDA1'] = [11, 22, 22, 33]
df[LDA2] = [33, 44, 55, 66]
......
"""


import time
import os.path
import math
from math import sqrt, pow, pi, log10, factorial, asin, degrees, radians

import numpy as np
from scipy import stats
import pandas as pd
import statsmodels.api as sm
import matplotlib.pyplot as plt

# 下面的这行代码目前存在问题,根据stackoverflow的评论,应该是pandas和matplotlib的一个bug
# http://stackoverflow.com/questions/33995707/attributeerror-unknown-property-color-cycle

#pandas给matplotlib提供了一种ggplot绘图的扩展主题(可以认为类似于windows的主题),效果更好看
# pd.set_option('display.mpl_style', 'default') # Make the graphs a bit prettier

#解决matplotlib中文问题
#注意:要放在pd.set_option的后面,否则pd.set_option会覆盖之前的matplotlib设置
import matplotlib as mpl
mpl.rcParams['font.family']='sans-serif'
mpl.rcParams['font.sans-serif'] = ['SimHei']  #指定默认字体
mpl.rcParams['axes.unicode_minus'] = False    #解决保存图像是负号'-'显示为方块的问题
# mpl.style.use('ggplot')

"""
关于绘图的废弃代码:
    #测试3种绘图方法
    #(1)直接绘制一个系列的数据
    #df.LDA2.cumsum().plot(logy=True)
    
    #(2)传统的绘图方法(利用matplotlib)
    #top = plt.subplot2grid((4,4), (0, 0), rowspan=2, colspan=4)
    #top.plot(df.index, df[u'LDA2'])
    #plt.title(u'LDA2')
    #bottom = plt.subplot2grid((4,4), (2,0), rowspan=2, colspan=4)
    #bottom.bar(df.index, df[u'LDA1'])
    #plt.title(u'LDA1')

    #(3)测试pandas提供的多图绘制方法
    #df.plot(subplots = True, figsize = (8, 8));
    #plt.legend(loc = 'best')

    #(4)计算移动平均(Moving average plot)
    #lda1_mavg = pd.rolling_mean(df.LDA1, 300)
    #df.LDA1.plot(label=u'LDA1', linewidth=3)
    #lda1_mavg.plot(label=u'LDA1_mavg')
    #plt.gcf().set_size_inches(15,8)

    #(5)某一系列数据的核密度估计(KDE)
    #df.LDA1[0:100].plot(kind='kde', label=u'LDA1-100')
    hist_plot(ts, 200)
    #df.LDA1.plot(kind='hist', stacked=True, bins=20)
    #df.LDA2.plot(kind='kde')
    #print df.LDA1.value_counts()
    #df.LDA1.value_counts().plot(kind='barh')

    #plt.gcf().set_size_inches(15,8)
    #print df.index
    #print df.columns
    #print df.describe()
    #print df[u'AT']

matplotlib中文显示问题
http://blog.csdn.net/rumswell/article/details/6544377
http://blog.sina.com.cn/s/blog_4d4afb6d010008xq.html
http://www.pythoner.com/200.html
http://blog.csdn.net/kongdong/article/details/4338826
"""

def curve_plot(ts):
    """
    绘制简单的曲线图.
    """
    #plt.grid(True)
    ts.plot()
    plt.xlabel(u'时间')
    plt.ylabel(u'速度')

"""
moving average -- 滑动平均
(1)MA－移平均线说明技术析领域移平均线(Moving Average)绝少指标工具 移平均线利用统计"移平均"原理每股价予移平均求趋势值用作股价走势研判工具
(2)滑动平均（moving average）：在地球物理异常图上，选定某一尺寸的窗口，将窗口内的所有异常值做算术平均，将所求的平均值作为窗口中心点的异常值。
按点距或线距移动窗口，重复此平均方法，直到对整幅图完成上述过程，这种过程称为滑动平均。
滑动平均相当于低通滤波，在重力勘探和测井的资料处理解释中常用此方法。[1] 
"""
def ma_plot(ts, freq=30, turbulence=True):
    # mag = pd.rolling_mean(ts, freq)    
    mag = ts.rolling(window=freq,center=False).mean()
    n = len(mag)
    if turbulence:
        start, end = n/3,2*n/3
        noise = np.sin(np.linspace(0,np.pi,end-start))
        mag[start:end] =  [y*(1+x)*2 for x,y in zip(noise, mag[start:end])]
    # mag.plot(linewidth=1, color='blue')
    plt.plot(np.linspace(0,10,n), mag,linewidth=0.5, color='black')
    plt.xlabel(u'时间')
    plt.ylabel(u'速度')
    #mag.plot(linestyle='--', linewidth=2, color='red')

def qq_plot(ts):
    """
    参考:
        http://stackoverflow.com/questions/13865596/quantile-quantile-plot-using-scipy
    检验是否服从正态分布：Q-Q图.
    以样本的分位数作为横坐标，以按照正态分布计算的相应分位点作为纵坐标，把样本表现为指教坐标系的散点。
    如果资料服从正态分布，则样本点应该呈一条围绕第一象限对角线(45度)的直线。
    """
    #plt.figure()
    #plt.grid(True)
    sm.qqplot(ts, line='45')
    plt.xlabel(u'分位数')
    plt.ylabel(u'分位数')
    plt.show()

def pp_plot(ts):
    """
    参考:
        http://stackoverflow.com/questions/13865596/quantile-quantile-plot-using-scipy
    检验是否服从正态分布：P-P图.
    以样本的累计频率作为横坐标，以安装正态分布计算的相应累计概率作为纵坐标，把样本值表现为直角坐标系中的散点。
    如果资料服从整体分布，则样本点应围绕第一象限的对角线分布。
    """
    #plt.grid(True)
    stats.probplot(ts, dist="norm", plot=plt)
    plt.xlabel(u'分位数概率')
    plt.ylabel(u'分位数概率')
    plt.show()

def kde_plot(ts):
    """
    参考:
        file:///D:/myfiles/%E7%BC%96%E7%A8%8B%E8%B5%84%E6%96%99/python/scipy-html/tutorial/stats.html#kernel-density-estimation
        http://pandas.pydata.org/pandas-docs/stable/visualization.html#visualization-kde
    检验是否服从正态分布: 核密度估计方法(kenerl density estimated).
    KDE与直方图都是用于统计某个值的出现次数,直方图是离散的,而KDE是连续的
    如果数据样本服从正态分布,图形会比较接近"锅盖型"
    """
    plt.figure()
    #plt.grid(True)
    ts.plot(kind='kde')
    plt.xlabel(u'速度')
    plt.ylabel(u'密度')
    plt.show()

def hist_plot(ts, bins):
    """
    检验是否服从正态分布: 直方图
    """
    # plt.figure()
    # #plt.grid(True)
    # ts.plot(kind='hist', stacked=True, bins=bins)
    # #ts.value_counts().plot()

    plt.figure()
    n, bins, patches = plt.hist(ts.values, bins=bins, normed=1)
    mu, sigma = ts.mean(), ts.std()
    y = stats.norm.pdf(bins, mu, sigma)
    plt.plot(bins, y, 'k--', linewidth=1.5)
    plt.xlabel(u'速度')
    plt.ylabel(u'密度')    
    plt.show()

"""
参考:
    http://wenda.tianya.cn/question/3ddb49fa4a9ae150
    file:///D:/myfiles/%E7%BC%96%E7%A8%8B%E8%B5%84%E6%96%99/python/scipy-html/tutorial/stats.html#t-test-and-ks-test
    http://stackoverflow.com/questions/10884668/two-sample-kolmogorov-smirnov-test-in-python-scipy

非参数检验方法包括Kolmogorov-Smirnov检验（D检验，也成为KS检验）和Shapiro- Wilk （W 检验）。
SAS中规定：当样本含量n ≤2000时，结果以Shapiro – Wilk（W 检验）为准，当样本含量n >2000 时，结果以Kolmogorov – Smirnov（D 检验）为准。
SPSS中则这样规定：
（1）如果指定的是非整数权重，则在加权样本大小位于3和50之间时，计算 Shapiro-Wilk 统计量。对于无权重或整数权重，
     在加权样本大小位于3 和 5000 之间时，计算该统计量。
（2）单样本 Kolmogorov-Smirnov 检验可用于检验变量（例如income）是否为正态分布。
对于此两种检验，如果P值大于0.05，表明资料服从正态分布。

The Kolmogorov-Smirnov test can be used to test the hypothesis that the sample comes from the standard t-distribution
Again the p-value is high enough that we cannot reject the hypothesis that the random sample really is distributed according to the t-distribution. 
In real applications, we don’t know what the underlying distribution is. If we perform the Kolmogorov-Smirnov test of our sample against the standard normal distribution, 
then we also cannot reject the hypothesis that our sample was generated by the normal distribution given that in this example the p-value is almost 40%.
"""

def ks_test_normal(ts, mu=0, sigma=1):
    """
    非参数检验法: K-S检验
    检验样本是否服从正态分布
    """
    #利用正态分布(5,10)进行K-S检验
    #d, pval = stats.kstest(ts,'norm', (5,10))
    #利用标准正态分布进行K-S检验
    d, pval = stats.kstest(ts,'norm', (mu, sigma))
    print u'K-S检验样本是否服从正态分布N(%f,%f):' % (mu, sigma)
    print u'    D = %6.3f pvalue = %6.4f' % (d, pval)
    d, pval = stats.kstest((ts-ts.mean())/ts.std(), 'norm')
    print u'标准化后检验样本是否服从标准正态分布N(0,1):'
    print u'    D = %6.3f pvalue = %6.4f' % (d, pval)

def ks_test_student(ts, freedom=10):
    """
    非参数检验法: K-S检验
    检验样本是否服从t分布(学生分布)
    """
    #X=stats.t.rvs(100,size=100)  #测试用
    d, pval = stats.kstest(ts, u't', (freedom,))
    print u'K-S检验样本是否服从t分布T(%f) D = %6.3f pvalue = %6.4f' % (freedom, d, pval)
    d, pval = stats.kstest((ts-ts.mean())/ts.std(), u't', (freedom,))
    print u'标准化后检验样本是否服从t分布T(%f) D = %6.3f pvalue = %6.4f' % (freedom, d, pval)

"""
检验2个样本是否服从同一分布
参考:
    file:///D:/myfiles/%E7%BC%96%E7%A8%8B%E8%B5%84%E6%96%99/python/scipy-html/tutorial/stats.html#kolmogorov-smirnov-test-for-two-samples-ks-2samp
"""

def ks_2samp(X1, X2):
    """
    非参数检验法: K-S检验
    检验2个样本是否服从同一部分
    """
    d, pval = stats.ks_2samp(rvs1, rvs2)
    print u'K-S检验2个样本是否服从同一分布 D = %6.3f pvalue = %6.4f' % (d, pval)

"""
参考:
    http://blog.sina.com.cn/s/blog_8ec0f0d80101cex0.html

1、covariance——协方差：两个不同变量间的方差，即两个随机变量与各自期望值的方差.
      Cov(Ri, Rj) = E{[Ri – E(Ri)][ Rj – E(Rj)]}
2、the properties of the covariance——协方差的特点
      1）协方差与方差是同一个概念，方差计算的是一个随机变量与其期望值的偏离程度，协方差计算的是两个随机变量之间的偏离程度。
      2）RA与其自身的协方差就是的方差，即 Cov（RA，RA）= Var（RA）
      3）协方差的取值范围为负无穷到正无穷

2、相关系数的特点
     1）相关系数表示两随机变量间的线性关系
     2）相关系数没有单位
     3）相关系数的取值范围为-1到1
     4）若Corr（Ri ,Rj）= 1.0，则表明两随机变量间为完全正相关，即一个随机变量变动时，另一个随机变量将向着相同的方向成比例变动。
     5）若Corr（Ri ,Rj）= -1.0，则表明两随机变量间为完全负相关，即一个随机变量变动时，另一个随机变量将向着相反的方向成比例变动。
     6）若Corr（Ri ,Rj）= 0，则表明两随机变量间无线性关系。
"""

def simple_plot(ts):
    """
    绘制基本曲线
    """
    #新建一个figure
    fig = plt.figure(figsize=(8, 6))

    #绘制简单曲线
    # curve_plot(ts)
    #滑动平均线(第2个参数代表取样个数)
    ma_plot(ts, freq=50, turbulence=False)

    #显示图形
    plt.show()
    #print u'\n'

def normal_plot(ts):
    """
    绘制3种统计曲线用于判断样本数据是否服从正态分布.
    """
    #新建一个figure
    #fig = plt.figure(figsize=(8, 6))
    #参数211的意思是：将画布分割成2行1列，图像画在从左到右从上到下的第1块
    
    #Q-Q图,判断是否服从正态分布
    #fig.add_subplot(311)
    qq_plot(ts)

    #P-P图,判断是否服从正态分布
    #fig.add_subplot(312)
    pp_plot(ts)
    
    #正态核密度估计,判断是否服从正态分布
    #fig.add_subplot(313)
    kde_plot(ts)
   
    hist_plot(ts, bins=50)
    #plt.legend()
    #显示图形
    #plt.show()
    # print u'\n'

def non_parameters_test(ts):
    #3、非参数检验
    #K-S检验标准正态分布
    ks_test_normal(ts)
    #K-S检验t分布(自由度等于10)
    #ks_test_student(ts, 10)
    # print u'\n'

def print_statstic(ts):
    """
    打印数据的统计信息.
    """
    #获取统计信息可以通过该函数一次性得到
    #   样本个数,最小最大, 均值, 方差, 偏度, 峰度
    #L, minmax, mean, std, skewness, kurtosis = stats.describe(ts)

    mu, sigma = ts.mean(), ts.std()
    P = stats.norm.cdf([ts.min(), ts.max()], mu, sigma)
    #print u'\t数据的概率之和(假设正态分布):',
    # for  d, p in zip(ts, P):
    #     print u'P(%f)=%f\n' % (d, p)
    print u'样本数据的一些统计量:\
            \n    样本数据个数:%d\
            \n    均值(Mean Value):%f\
            \n    中位数(Median Value):%f\
            \n    均方差(Std Var):%f\
            \n    最小值(Min Value):%f\
            \n    最大值(Max Value):%f\
            \n    偏度(Skewness):%f\
            \n    峰度(Kurtosis):%f\
            \n    随机变量落在区间(%f, %f)的概率(假设正态分布):%f' \
            % (len(ts), ts.mean(), ts.median(), ts.std(), ts.min(), ts.max(), ts.skew(), ts.kurtosis(), ts.min(), ts.max(), P[1]-P[0])

def get_plot_param(ts, data_range):
    first, last, step, color, linewidth, linestyle = 0, len(ts), 100, 'blue', 1.0, '-'

    if data_range is None or len(data_range)==0:
        return first, last, step, color, linewidth, linestyle

    if u'first' in data_range:
        first = data_range[u'first']

    if u'last' in data_range:
        last = data_range[u'last']

    if u'step' in data_range:
        step = data_range[u'step']

    if u'color' in data_range:
        color = data_range[u'color']

    if u'linewidth' in data_range:
        linewidth = data_range[u'linewidth']

    if u'linestyle' in data_range:
        linestyle = data_range[u'linestyle']

    return first, last, step, color, linewidth, linestyle

def pvalue_f(ts):
    ks = lambda x : stats.kstest((x-x.mean())/x.std(), 'norm')[1]
    return ks(ts)
def mean_f(ts):
    return ts.mean()
def std_f(ts):
    return ts.std()

func_labels = {
    pvalue_f : u'K-S检验P值',
    mean_f : u'样本均值',
    std_f : u'样本均方差'
}

def plot_wraper(ts, func, data_ranges=None):
    DataRanges = data_ranges
    if DataRanges is None or len(DataRanges)==0:
        DataRanges = [None]
    for data_range in DataRanges:
        first, last, step, color, linewidth, linestyle = get_plot_param(ts, data_range)
        n = (last - first) / step
        X = [(i+1)*step+first for i in range(n)]
        Y = [func(ts[first:(i+1)*step+first]) for i in range(n)]
        plt.xlabel(u'时间')
        plt.ylabel(func_labels[func])
        plt.plot(X, Y, color=color, linewidth=linewidth, linestyle=linestyle)

def CaclPlotRowColNum(n):
    if n == 1:
        #1*1
        return 111
    elif n == 2:
        #2*1
        return 211
    elif n == 3:
        #3*1
        return 311
    elif n == 4:
        #2*2
        return 221
    elif n == 5:
        #3*2
        return 321
    elif n == 6:
        #3*2
        return 321
    elif n == 7:
        #4*2
        return 241
    elif n == 8:
        #4*2
        return 421
    elif n == 9:
        #3*3
        return 331
    else:
        #3*3
        return 331
"""
参考：
    matplotlib的绘图样式(plot style)参数
    http://www.cnblogs.com/xilifeng/p/3917616.html
"""
def stats_var_plot(ts, plot_funcs, step=100, bPlotInOne=False,data_ranges=None):
    """
    绘制统计量的变化曲线
    """
    if len(plot_funcs) == 0:
        return

    if data_ranges is None or len(data_ranges)==0:
        data_ranges = [{u'step':step}]

    plotNum = CaclPlotRowColNum(len(plot_funcs))
    if bPlotInOne:
        fig = plt.figure()
    for plotf in plot_funcs:
        if bPlotInOne:
            fig.add_subplot(plotNum)
            plotNum = plotNum + 1
        if not bPlotInOne:
            fig = plt.figure()
        plot_wraper(ts, plotf, data_ranges)
        if not bPlotInOne:
            plt.show()
    if bPlotInOne:
        plt.show()

def time_statstic_plot(T, step=100):
    """
    绘制一定步长时间内数据点的个数
    """
    min_t, max_t = T.min(),T.max()
    print u'样本数据个数:%d, 开始时间:%f, 结束时间:%f' % (len(T), min_t, max_t)

    N=int(max_t)/step
    times = [i*step for i in range(N+1)]
    count_f = lambda t : len(T[T<t])
    freqs = [count_f(t+step) for t in times]
    widths = [step for t in times]
    if N*step < max_t:
        widths[-1] = max_t-N*step

    prev_fq =lambda i : 0 if i<0 else freqs[i] 
    freqs = [fq-prev_fq(i-1) for i,fq in enumerate(freqs)]

    plt.xlabel(u'时间')
    plt.ylabel(u'数据密度')

    #绘制直方图
    # fig = plt.figure()
    # fig.add_subplot(211)
    plt.bar(times, freqs, widths)

    #绘制曲线
    # fig.add_subplot(212)
    times = [t+step for t in times]
    if N*step < max_t:
        times[-1] = max_t
    #如果不需要可以注释掉下面的一行代码
    plt.plot(times, freqs, color='blue', linestyle='-', linewidth=1.5)

    plt.show()

"""
http://stackoverflow.com/questions/9456037/scipy-numpy-fft-frequency-analysis
http://stackoverflow.com/questions/21517998/python-scipy-fftpack-rfft-frequency-bin-mapping
http://docs.scipy.org/doc/numpy/reference/generated/numpy.fft.fft.html#numpy.fft.fft
"""
def fft_test(signal):
    import scipy.fftpack

    #使用scipy中的fft
    FFT = abs(scipy.fft(signal))
    freqs = scipy.fftpack.fftfreq(signal.size, d=1/33.34)
    plt.plot(freqs,20*scipy.log10(FFT),'x')
    #plt.plot(freqs,FFT,'x')
    plt.show()

def fft_test2():
    """
    傅立叶变换测试
    挺有意思的~
    """
    #使用numpy中的fft
    t = np.arange(256)
    sp = np.fft.fft(np.sin(t))
    freq = np.fft.fftfreq(t.shape[-1])
    plt.plot(freq, sp.real, freq, sp.imag)
    plt.show()

def fft_test3():
    #使用numpy中的fft
    n = np.arange(64)
    f1, f = 1000,3000
    pi = np.pi
    s1 = np.sin(2*pi*f1/f*n)
    sp = np.fft.fft(s1)
    #freq = np.fft.fftfreq(n.shape[-1])
    #plt.plot(freq, sp.real, freq, sp.imag)
    plt.plot(np.abs(sp))
    plt.show()

def read_excel_data(xls_file, sheet_name, header=7):
    """
    从xls文件读取数据.
    xls_file:
        excel文件
    sheet_name:
        xls文件的sheet
    head_index:
        数据从哪一行开始(1, 2, ....)
    """
    #这2种方法是等价的
    #return pd.read_excel(xls_file, sheet_name, skiprows=range(head_index-1))
    df = pd.read_excel(xls_file, sheet_name, header=header-1)
    print u'读取xls文件:'
    print u'    路径:', xls_file
    print u'    数据个数:', len(df.index)
    print u'    所有数据列:', list(df.columns)
    print u'    表名:', sheet_name
    print u'    标题行:', header
    return df

"""
废弃代码:
    #以当前时间作为随机数的种子
    #np.random.seed(time.time())
    #生成一组均匀分布的随机数
    x = np.random.random(n)
    #生成一组服从t分布的数据
    #x = stats.t.rvs(5, size=n)
    #生成一组服从标准正态分布的数据(n个数据)
    #x = stats.norm.rvs(size=n)
    #生成一组服从正态分布的数据
    #mu, sigma =5, 10
    #x = stats.norm.rvs(loc=mu, scale=sigma, size=n)

    #xs = np.linspace(x1.min() - 1, x1.max() + 1, 200)
    #用正态分布的pdf函数计算样本的概率密度
    #ys = stats.norm.pdf(xs)
    #用t分布的pdf函数计算样本的概率密度
    #ys = stats.t.pdf(xs, 5)
"""
def generate_random_data(n, **args):
    
    freedom, mu, sigma = 10, 0.0, 1.0

    if u'freedom' in args:
        freedom = args[u'freedom']
    if u'mu' in args:
        mu = args[u'mu']
    if u'sigma' in args:
        sigma = args[u'sigma']
    
    dist_type = u'norm'
    if u'dist_type' in args:
        dist_type = args[u'dist_type']

    if dist_type == u'norm':
        return stats.norm.rvs(size=n, loc=mu, scale=sigma)
    elif dist_type == u't':
        return stats.t.rvs(freedom, size=n)
    else:
        return np.random.random(n)

#主函数
def main():
    # fft_test3()
    #xxx()

    print u'读取数据.....'
    #从xls文件中读取多列数据(BSA Export表格中第7行开始,下面2行代码是等价的)
    #返回的D是一个词典
    df = read_excel_data(u'data2.xls', u'BSA Export', 7)
    #计算相关系数
    #df.corr()
    #计算协方差
    #df.cov()
    #D = generate_data1(u'data3.xls', [u'U', u'V', u'W'], u'BSA Export', 7)
    #取某一列数据用于下面的测试
    ts = df[u'LDA1']
    T = df[u'AT2']+df[u'TT2']

    #随机生成一组数据
    #ts = generate_random_data(30000, dist_type=u'random')  #均匀分布
    #ts = generate_random_data(30000, dist_type=u'norm', mu=0, sigma=1) #正态分布
    #ts = generate_random_data(30000, dist_type=u't', freedom=10)  #t分布

    #绘制时间
    time_statstic_plot(T, step=1000)
    #傅立叶测试
    fft_test(ts)

    print u'打印统计数据'
    print_statstic(ts[10:100])   # 只统计第10~100个数据
    #print_statstic(ts)
    
    # print u'绘制数据曲线...'
    simple_plot(ts)

    # print u'绘图判断是否正态分布...'
    normal_plot(ts)
    
    # print u'非参数检验是否服从正态分布...'
    non_parameters_test(ts)

    # print u'绘制统计量的变化曲线...'
    #如果定义了data_ranges,则在一个图中绘制不同区间的均值变化曲线
    data_ranges = [
        {u'first':0, u'last':10000, u'step':100, u'color':'blue', u'linewidth':1, u'linestyle':"-"},
        {u'first':1000, u'last':5000, u'step':150, u'color':'red', u'linewidth':2, u'linestyle':"--"},
        {u'first':2000, u'last':7000, u'step':100, u'color':'green', u'linewidth':1, u'linestyle':":"}
    ]
    #stats_var_plot(ts, [pvalue_f, mean_f, std_f], bPlotInOne=False, step=100)
    stats_var_plot(ts, [pvalue_f, mean_f, std_f], bPlotInOne=False, step=100, data_ranges=data_ranges)

#主函数入口
if __name__=="__main__":
    main()

